package ua.com.socialtours.bot.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/*
 * Written by Denys Yashchuk denys.yashchuk@gmail.com, Jul 2020
 */
@FeignClient(name = "TelegramClient", url = "https://api.telegram.org/botId:botKey")
public interface TelegramClient  {

    @GetMapping("/sendMessage")
    ResponseEntity<String> sendMessage(@RequestParam(name="chat_id") @NonNull final String chatId,
                                       @RequestParam(name="text") @NonNull final String text);

}
