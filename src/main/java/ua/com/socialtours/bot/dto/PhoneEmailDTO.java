package ua.com.socialtours.bot.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

/*
 * Written by Denys Yashchuk denys.yashchuk@gmail.com, Jul 2020
 */
@Getter
@Builder(toBuilder = true)
@AllArgsConstructor
@ToString
public class PhoneEmailDTO {

    private final String phone;
    private final String email;

    public PhoneEmailDTO() {
        this.phone = "";
        this.email = "";
    }
}
