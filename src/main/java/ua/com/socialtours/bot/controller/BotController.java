package ua.com.socialtours.bot.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ua.com.socialtours.bot.dto.PhoneEmailDTO;
import ua.com.socialtours.bot.service.SocialToursChatTelegramBot;

/*
 * Written by Denys Yashchuk denys.yashchuk@gmail.com, Jul 2020
 */
@RequiredArgsConstructor
@RestController
@RequestMapping("/api")
public class BotController {

    @NonNull
    private final SocialToursChatTelegramBot socialToursChatTelegramBot;

    @PostMapping("/send")
    public ResponseEntity<String> send(@RequestBody @NonNull final PhoneEmailDTO phoneEmailDTO){
        return new ResponseEntity<>(socialToursChatTelegramBot.sendMessageToChat(phoneEmailDTO), HttpStatus.OK);
    }

}
