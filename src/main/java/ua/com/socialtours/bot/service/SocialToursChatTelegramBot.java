package ua.com.socialtours.bot.service;

import org.springframework.lang.NonNull;
import ua.com.socialtours.bot.dto.PhoneEmailDTO;

/*
 * Written by Denys Yashchuk denys.yashchuk@gmail.com, Jul 2020
 */
public interface SocialToursChatTelegramBot {

    String sendMessageToChat(@NonNull final PhoneEmailDTO text);

}
