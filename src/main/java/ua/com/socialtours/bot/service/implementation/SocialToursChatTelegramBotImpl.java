package ua.com.socialtours.bot.service.implementation;

import lombok.RequiredArgsConstructor;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import ua.com.socialtours.bot.client.TelegramClient;
import ua.com.socialtours.bot.dto.PhoneEmailDTO;
import ua.com.socialtours.bot.service.SocialToursChatTelegramBot;

/*
 * Written by Denys Yashchuk denys.yashchuk@gmail.com, Jul 2020
 */
@RequiredArgsConstructor
@Service
public class SocialToursChatTelegramBotImpl implements SocialToursChatTelegramBot {

    @NonNull
    private final TelegramClient telegramClient;

    @Override
    public String sendMessageToChat(@NonNull final PhoneEmailDTO text) {
        return telegramClient.sendMessage("chatId", "Я хочу з вами в тур. Подзвоніть: " +
                text.getPhone() + ", чи напишіть: " + text.getEmail()).getBody();
    }
}
